#!/bin/sh

go_version="$(which go > /dev/null && go version | cut -d' ' -f3)"
go_latest="$(curl -s https://go.dev/VERSION?m=text | head -n1)"

if [ "${go_version}" != "${go_latest}" ]; then
  echo "upgrading go installation";
  rm -rf /usr/local/go
  curl -s https://dl.google.com/go/${go_latest}.linux-amd64.tar.gz | tar -xzC /usr/local
  export PATH=/go/bin:/usr/local/go/bin:$PATH
fi

go install golang.org/x/vuln/cmd/govulncheck@latest
