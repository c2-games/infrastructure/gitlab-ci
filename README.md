# Gitlab CI Templates

A collection of Gitlab CI templates for use in the C2Games projects

## Documentation

Read the docs at https://gitlab-ci.c2games.org/

Or locally [here](docs/README.md)

## Pre-commit Usage

### Requirements

- docker
- python3
- python3-venv

Install pre-commit python binary in virtualenv and symlink to the user bin path.

*Note: This is only one way to install the `pre-commit` binary. Other methods of installation are available at [pre-commit.com/#install](https://pre-commit.com/#install)*

```sh
$ cd
$ mkdir python-envs
$ cd python-envs
$ python3 -m venv pre-commit
$ . ./pre-commit/bin/activate
(pre-commit) $ pip install -U pip pre-commit
Installing collected packages: distlib, pyyaml, platformdirs, nodeenv, identify, filelock, cfgv, virtualenv, pre-commit
Successfully installed cfgv-3.4.0 distlib-0.3.8 filelock-3.13.1 identify-2.5.33 nodeenv-1.8.0 platformdirs-4.1.0 pre-commit-3.5.0 pyyaml-6.0.1 virtualenv-20.25.0

(pre-commit) $ pre-commit --version
pre-commit 3.5.0

(pre-commit) $ ln -s $(which pre-commit) ~/bin/
(pre-commit) $ deactivate

$ pre-commit --version
pre-commit 3.5.0
```

### Create pre-commit config in repo

`/path/to/repo/.pre-commit-config.yaml`

```yaml
---
# See https://pre-commit.com for more information
# See https://pre-commit.com/hooks.html for more hooks
repos:
  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v4.5.0
    hooks:
      - id: trailing-whitespace
      - id: end-of-file-fixer
      - id: check-added-large-files
      - id: detect-private-key
      - id: no-commit-to-branch
        args:
          - --branch
          - main

  - repo: https://github.com/bridgecrewio/checkov
    rev: 3.1.61
    hooks:
      - id: checkov_secrets_container

  - repo: https://gitlab.com/c2-games/infrastructure/gitlab-ci.git
    rev: "v1.0.0"
    hooks:
      - id: docker-cspell
      - id: docker-yamllint
      - id: docker-markdownlint
      - id: docker-hadolint
      - id: docker-trivy
      - id: docker-terraform-fmt
      - id: docker-terraform-docs
        # Overide default parameters for the docker-terraform-docs hook
        files: ^iac/
        exclude: ^iac/modules
        args:
          - table
          - --sort-by
          - required
          - --output-file
          - README.md
          - .
...

```

### Install pre-commit hooks

```sh
$ pre-commit install && pre-commit install-hooks
pre-commit installed at .git/hooks/pre-commit
```

### Run pre-commit hooks manually

Running `pre-commit --all-files` will manually run all hooks defined in `.pre-commit-config.yaml`

```sh
$ pre-commit run --all-files
trim trailing whitespace.................................................Passed
fix end of files.........................................................Passed
check for added large files..............................................Passed
detect private key.......................................................Passed
don't commit to branch...................................................Passed
Checkov Secrets..........................................................Passed
CSpell Spellcheck........................................................Passed
YAML Lint................................................................Passed
Trivy....................................................................Passed
```
