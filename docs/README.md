# Gitlab CI Templates

## Usage Examples

```yaml
include:
  - "https://gitlab-ci.c2games.org/lint/python.yml"
  - "https://gitlab-ci.c2games.org/build/python.yml"
  - "https://gitlab-ci.c2games.org/test/python.yml"
  - "https://gitlab-ci.c2games.org/publish/pypi.yml"

variables:
  PYTHON_IMAGE: "docker.io/python:3.11"
```

## Global Variables

| Key               | Value                   | Description           |
| :---------------- | :---------------------- | :-------------------- |
| PYTHON_IMAGE      | docker.io/python:latest | Python container path |
| GOLANG_IMAGE      | docker.io/golang:latest | Golang container path |
| DEPLOYMENT_TARGET | development             | Prod vs Dev           |
